import { Student } from "../schema/model.js";

export let createStudent = async (req, res, next) => {
  let data = req.body;

  try {
    let result = await Student.create(data);
    res.json({
      success: true,
      message: "Student created successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let getStudent = async (req, res, next) => {
  try {
    // let result = await Student.find({});
    // let result = await Student.find({name:"nitan"});
    //while searching we only focus on value  (we do not focus on type)
    // let result = await Student.find({ name: "nitan", roll: 50 });

    //number searching
    // let result = await Student.find({ roll: 70});
    // let result = await Student.find({ roll: { $gt: 70 } });
    // let result = await Student.find({ roll: { $gte: 70 } });
    // let result = await Student.find({ roll: { $lt: 70 } });
    // let result = await Student.find({ roll: { $lte: 70 } });
    // let result = await Student.find({ roll: { $ne: 70 } });
    // let result = await Student.find({ roll: { $in: [20, 25, 30] } });
    // let result = await Student.find({ roll: { $gte: 20, $lte: 25 } });

    //string searching
    // let result = await Student.find({ name: { $in: ["nitan", "ram"] } });

    //regex searching = not exact searching

    // let result = await Student.find({name:"nitan"})
    // let result = await Student.find({name:/nitan/})
    // let result = await Student.find({name:/nitan/i})
    // let result = await Student.find({name:/ni/})
    // let result = await Student.find({name:/ni/i})
    // let result = await Student.find({name:/^ni/})
    // let result = await Student.find({name:/^ni/i})
    // let result = await Student.find({name:/ni$/})

    // select
    // find has control over the object where as select as control over the object properties
    // let result = await Student.find({}).select("name gender -_id");
    // let result = await Student.find({}).select("name gender age");
    // let result = await Student.find({}).select("-name -gender ");
    // let result = await Student.find({}).select(" name -gender ");//it is not valid
    //  in select use either all -    or use all +  but do not use both except _id

    //sorting

    //skip

    let result = await Student.find({}).skip("2");

    //limit

    // let result = await Student.find({}).limit("2");

    res.json({
      success: true,
      message: "Student read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: "Unable to read Student",
    });
  }
};

export let getSpecificStudent = async (req, res, next) => {
  let id = req.params.id;

  try {
    let result = await Student.findById(id);
    res.json({
      success: true,
      message: "Student read successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let updateStudent = async (req, res, next) => {
  let id = req.params.id;
  let data = req.body;

  try {
    let result = await Student.findByIdAndUpdate(id, data, { new: true });
    // if {new:false}  it give old data
    // if {new:true} it give new data
    res.json({
      success: true,
      message: "Student updated successfully.",
      result: result,
    });
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};

export let deleteStudent = async (req, res, next) => {
  let id = req.params.id;
  try {
    let result = await Student.findByIdAndDelete(id);

    if (result === null) {
      res.json({
        success: false,
        message: "Student does not exist",
      });
    } else {
      res.json({
        success: true,
        message: "Student deleted successfully.",
        result: result,
      });
    }
  } catch (error) {
    res.json({
      success: false,
      message: error.message,
    });
  }
};



