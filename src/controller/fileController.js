export let uploadSingleFile = (req, res, next) => {
  console.log(req.file);
  res.json({
    success: true,
    message: "file upload successfully",
    link: `http://localhost:8000/${req.file.filename}`,
  });
};

export let uploadMultipleFiles = (req, res, next) => {
  let link = req.files.map((value, i) => {
    return `http://localhost:8000/${value.filename}`;
  });
  res.json({
    success: true,
    message: "file upload successfully",
    link: link,
  });
};

// {
//   success:true,
//   message:"file uploaded successfully"
//   link:["http://localhost:8000/.....", "http://localhost:8000/.....","http://localhost:8000/....."]
// }[

/* 
[
  "http://localhost:8000/1707118979767Rectangle 6.png",
  "http://localhost:8000/1707118979770Rectangle 4.jpg",
  "http://localhost:8000/1707118979782r1.png",
]
 */
