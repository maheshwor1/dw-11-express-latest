import { Router } from "express";
import {
  createReview,
  deleteReview,
  readAllReview,
  readSpecificReview,
  updateReview,
} from "../controller/reviewController.js";
//import { createReview, deleteReview, readAllReview, readSpecificReview, updateReview } from "../controller/reviewController.js";
// import { createReview, deleteReview, readAllReview, readSpecificReview, updateReview } from "../controller/reviewController.js";

export let reviewRouter = Router();

reviewRouter.route("/").post(createReview).get(readAllReview);

reviewRouter
  .route("/:id") //localhost:8000/students/1234124
  .get(readSpecificReview)
  .patch(updateReview)
  .delete(deleteReview);

/* 
  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  
  
  */
