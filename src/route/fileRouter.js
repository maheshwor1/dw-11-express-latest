import { Router } from "express";
import {
  uploadMultipleFiles,
  uploadSingleFile,
} from "../controller/fileController.js";
import upload from "../utils/fileUpload.js";

export let fileRouter = Router();

fileRouter.route("/single").post(upload.single("profile"), uploadSingleFile);
fileRouter
  .route("/multiple")
  .post(upload.array("profile"), uploadMultipleFiles);

/* 



  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  */

/* 

  {
    success:true,
    message:"file uploaded successfully"
    link:"http://localhost:8000/....."
  }


   {
    success:true,
    message:"file uploaded successfully"
    link:[
      " http://localhost:8000/.....",
      " http://localhost:8000/.....",
    ]
  
  }
  
  */
