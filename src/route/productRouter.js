import { Router } from "express";
import { createProduct, deleteProduct, readAllProduct, readSpecificProduct, updateProduct } from "../controller/productController.js";


export let productRouter = Router();

productRouter.route("/").post(createProduct).get(readAllProduct);

productRouter
  .route("/:id") //localhost:8000/students/1234124
  .get(readSpecificProduct)
  .patch(updateProduct)
  .delete(deleteProduct);

/* 
  Student.create(data)
  Student.find({})
  Student.findById(id)
  Student.findByIdAndUpdate(id,data)
  Student.findByIdAndDelete(id)
  
  
  */
