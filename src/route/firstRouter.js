import { Router } from "express";
export let firstRouter = Router();

firstRouter
  .route("/") //localhost:8000/bike
  .post(
    (req, res, next) => {
      console.log("i am middleware 1");
      req.name = "nitan";
      next();
    },
    (req, res, next) => {
      console.log(req.name);
      console.log("i am middleware 2");
    }
  )
  .get(
    (req, res, next) => {
      console.log("i am middleware 1");
      let error = new Error("my error");
      next(error);
    },
    (err, req, res, next) => {
      console.log(err.message);
      console.log("i am error middleware");
      next();
    },
    (req, res, next) => {
      console.log("i am middleware 2");
    }
  )
  .patch((req, res, next) => {
    res.json("bike patch");
  })
  .delete((req, res, next) => {
    res.json("bike delete");
  });

/* 


      url=localhost:8000/bike,post at response "bike post"
			url=localhost:8000/bike,get at response "bike get"
			url=localhost:8000/bike,patch at response "bike patch"
			url=localhost:8000/bike,delete at response "bike delete"
 */

/* 

   
      
      
      
      
      */
