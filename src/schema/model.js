//defining array is called mode
//name
//object

import { model } from "mongoose";
import { studentSchema } from "./studentSchema.js";
import { teacherSchema } from "./teacherSchama.js";
import { contactSchema } from "./contactSchama.js";
import { collegeSchema } from "./collegeSchema.js";
import { productSchema } from "./productSchema.js";
import { userSchema } from "./userSchema.js";
import { reviewSchema } from "./reviewSchema.js";

export let Student = model("Student", studentSchema);
export let Teacher = model("Teacher", teacherSchema);
export let Contact = model("Contact", contactSchema);
export let College = model("College", collegeSchema);
export let Product = model("Product", productSchema);
export let User = model("User", userSchema);
export let Review = model("Review", reviewSchema);

//model name  must be singular
