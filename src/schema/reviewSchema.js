import { Schema } from "mongoose";

export let reviewSchema = Schema({
  productID: {
    type: Schema.ObjectId,
    ref: "Product",
    required: true,
  },
  userID: {
    type: Schema.ObjectId,
    ref: "User",
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});
